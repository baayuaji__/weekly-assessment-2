# **Weekly Assessment 2**
###### _Name: Bayu Aji Pamungkas_
###### _Class: BE Morning Batch 16_

**1. Explain How the Internet Works**
Internet adalah infrastruktur besar yang menghubungkan miliaran komputer di seluruh dunia. Ketika satu komputer ingin terhubung dengan komputer lain, saluran fisik (kabel Ethernet) atau saluran non-fisik seperti WiFi atau Bluetooth dapat digunakan untuk mengirim informasi. Tetapi akan rumit untuk menghubungkan sepuluh komputer atau lebih menggunakan kabel, oleh karena itu akan digunakan router. Router adalah komputer kecil khusus yang berfungsi sebagai mediator bagi banyak komputer yang terhubung ini, untuk memastikan bahwa setiap informasi yang dikirim dari satu komputer mencapai komputer tujuan.

Router yang menghubungkan beberapa komputer, dapat dihubungkan ke router lain untuk memperlebar konektivitas komputer tersebut. Kumpulan router (jaringan) dapat dihubungkan ke grup router lain melalui ISP (Internet Service Provider). ISP dikelola oleh perusahaan layanan, dan terhubung ke ISP lain yang terdiri dari kelompok router, membentuk jaringan komputer global yang sangat besar. Untuk memastikan informasi mencapai tujuan yang benar, untuk setiap komputer disediakan sebuah alamat unik yang disebut Alamat IP, yakni sebuah rangkaian empat angka yang dipisahkan oleh titik. Karena IP Address sangat sulit untuk dihafal oleh manusia, IP Address dapat memiliki nama alias yang disebut Domain, agar lebih mudah dipahami dan dihafal oleh manusia. Contoh nama domain adalah Google.com, Facebook.com, Codewars.com, dll.

Jadi melalui sistem infrastruktur yang begitu besar, satu komputer dari satu belahan dunia dapat berkomunikasi dan mengirim ribuan informasi ke komputer lain di sisi lain dunia secara jarak jauh. Begitulah cara kerja internet.

**2. What's the meaning of communication protocol**
_Communication Protocol_ adalah sebuah prosedur atau peraturan yang harus dipatuhi dalam proses hubungan, komunikasi, transfer data antar dua piranti elektronik atau lebih. Terdapat tiga jenis utama protokol jaringan.

- **Network Communication Protocols**
Protokol ini memberikan aturan dan format mengenai bagaimana data ditransfer dalam sebuah jaringan, dan juga menangani otentikasi dan pendeteksian error.
**HTTP** = singkatan Hypertext Transfer Protocol, adalah protocol internet yang bekerja di lapisan aplikasi (application layer) yang memungkinkan browser dan server berkomunikasi,
**TCP** = singkatan Transmission Control Protocol yang memecah data menjadi packets atau paket-paket kecil sehingga dapat dibagikan dan ditransfer antar jaringan. 

- **Network Management Protocols**]
Protokol berikut ini memberikan aturan dan prosedur dalam memonitor, mengatur, dan me-maintain jaringan komputer, dan membantu mengkomunikasikan keperluan tersebut ke seluruh jaringan guna memastikan komunikasi yang stabil dan optimal antar perangkat.
**SNMP** = singkatan dari Simple Network Management Protocol (SNMP) yang digunakan untuk memonitor dan mengatur perangkat jaringan, yakni membiarkan administrator untuk melihat dan memodifikasi endpoint informations di dalam jaringan.
**ICMP** = singkatan dari Internet Control Message Protocol (ICMP) digunakan untuk tujuan diagnosis/pengecekan. Perangkat dalam jaringan dapat menggunakan protokol berikut untuk mengirimkan pesan error, menyediakan informasi perihal masalah keterhubungan jaringan antar perangkat.
- **Network Security Protocols**
Protokol ini berfungsi untuk memastikan perpindahan data dalam koneksi jaringan tetap aman dan terlindungi. Protokol ini membantu memastikan agar tak ada pengguna, server, ataupun perangkat yang tak diizinkan, dapat mengakses data jaringan.
**SSL** = singkatan Secure Socket Layer digunakan untuk memastikan koneksi internet yang aman dan melindungi data yang sensitif.Data yang ditransfer menggunakan SSL akan dienkripsi agar mencegah data tersebut dapat dibaca.
**HTTPS** = singkatan dari Secure Hypertext Transfer Protocol, merupakan versi aman dari HTTP, yang digunakan untuk mengenkripsi data yang dikirimkan antara browser dan server.


**3. Mention HTTP status codes**

HTTP status code adalah satu seri kode terdiri dari tiga angka yang dikirimkan sebagai pertanda apakah sebuah HTTP Request telah berhasil dijalankan atau tidak. HTTP Status Codes dapat dikelompokkan menjadi 5 berdasarkan jenis pesan yang dikirimkan.
1. Informational Responses (dari 1oo sampai 199)
2. Successful Responses (dari 200 sampai 299)
3. Redirection Messages (dari 300 sampai 399)
4. Client Error Responses (dari 400 sampai 499)
5. Server Error Responses (dari 500 sampai 599)

Adapun contoh beberapa status kode yang penting untuk diketahui ialah sebagai berikut:
200 = Kode 200 adalah standard untuk request HTTP yang berhasil, dimana respon yang dikembalikan bergantung pada jenis requestnya.
201 = Mengkonfirmasi bahwa request telah berhasil dan sebagai hasilnya, sebuah sumber daya baru telah dibuat. 
204 = Mengkonfirmasi bahwa server telah memenuhi request namun tidak perlu mengembalikan informasi apapun. 
301 = Menandakan bahwa URL sumber daya yang diminta telah diubah secara permanen. URL yang baru akan diberikan sebagai responnya.
307 = server akan mengarahkan klien ke URI lain untuk memperoleh sumber daya yang telah di-request.
400 = Server gagal untuk memproses request yang disebabkan oleh client error, seperti _missing data, domain validation, dan invalid formatting._
401 = Kode 401 dikirim ketika otentikasi yang diperlukan gagal atau tidak diberikan.
403 =  Mirip seperti 401, namun server menolak otentikasi karena client tidak memiliki izin/permission yang diperlukan untuk mengakses sumber daya
404 = 404 akan dikirimkan ketika request yang dilakukan valid tetapi sumber daya tidak tersedia/ditemukan di server
500 = 500 muncul ketika server tidak dapat memenuhi request akibat kesalahan yang tidak diketahui.

**4. Make Analogy Regarding how Front end and Backend communicate each other**

Cara bagaimana Front End dan Back End berkomunikasi satu sama lain dapat diibaratkan melalui analogi sebuah restoran. Di setiap restoran, ada bagian yang berhubungan langsung dengan pelanggan/client, yakni bagian depan restoran dimana pelanggan duduk, melakukan order, dan menyantap makanan yang telah dipesan. Inilah tugas Frond End, yakni mempersiapkan suatu halaman aplikasi web yang diakses dan menerima pesanan dari client. Segala macam tampilan "restoran" (User Interface) dan pengalaman makan di "restoran" tersebut (User Experience) menjadi tanggung jawab dari Frond End. Sementara itu, pelanggan tidak hanya menikmati duduk dibagian depan restoran saja tetapi juga memesan makanan yang ia santap. Pesanan yang ia minta dari pelayan akan dibawa ke dapur Backend untuk diterima dan disajikan kembali ke pada pelanggan. Inilah tugas dari Backend. Backend sebagai juru dapur data bertugas menyediakan apa yang diminta oleh pelanggan. Dibalik yang dikerjakan Front End, sebagai koki, Back End bertanggung jawab dalam meracik data yang lengkap dan jelas, sesuai dengan pesanan, sehingga dapat dinikmati pelanggan dengan nikmat..

**5. Mention What method in HTTP protocol**

Secara resmi, tercatat terdapat 39 verbs/methods yang ada di Protocol HTTP, akan tetapi ada 4 metode utama yang paling sering digunakan oleh para developer.
4 metode tersebut antara lain GET, POST, PUT, dan DELETE. Selengkapnya akan dibahas di bawah ini.
GET = Metode Get digunakan untuk membaca, mengambil/READ suatu representasi dari sumber daya yang diinginkan. Bila sukses, maka akan dikembalikan status code 200 (OK), namun bila error, maka seringkali ditampilkan status code 404(Not Found) atau 400 (Bad Request).
POST = Metode POST sering digunakan untuk membuat/CREATE suatu sumber daya baru, semisal membuat ID baru dst. Bila berhasil, maka ditampilkan status code 201.
PUT = Metode PUT bisa dibilang berkebalikan dengan metode GET, dimana GET me-request sumber daya tertentu, PUT menerima dan memodifikasi/meng-UPDATE suatu input/sumberdaya itu ke dalam remote directory. PUT mengasumsikan bahwa sumber daya tersebut belum eksis/ada atau sudah ada tapi dapat ditimpa/overwritten di database. 
DELETE = Metode DELETE digunakan untuk menghapus sumberdaya yang dipilih/ditarget. Jika berhasil, respon umum terhadap metode DELETE ialah status code 200 "OK" atau 204 "No Content" dimana sumberdaya nya telah berhasil dihapus.
